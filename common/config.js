const config = {
    HOST: process.env.HOST || "localhost",
    PORT: process.env.PORT || "8000",
    MYSQL_HOST: process.env.MYSQL_HOST,
    MYSQL_PORT: process.env.MYSQL_PORT,
    MYSQL_USER: process.env.MYSQL_USER,
    MYSQL_PWD: process.env.MYSQL_PWD,
    MYSQL_DB: process.env.MYSQL_DB
}

module.exports = config;
