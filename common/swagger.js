const swaggerDoc = {
    swaggerDefinition: {
        info: {
            description: 'Serveur de gestion des utilisateurs',
            title: 'EcoSurf - Gestion Utilisateur',
            version: '1.0.0',
        },
        host: `${process.env.HOST || 'localhost'}:${process.env.PORT || '3000'}`,
        basePath: '',
        produces: [
            "application/json",
        ],
        schemes: ['http', 'https'],
    },
    basedir: __dirname, //app absolute path
    files: ['../api/routers/**/*.js', '../api/models/**/*.js'] //Path to the API handle folder
};

module.exports = swaggerDoc;
