const db = require("../models");
const Category = db.category;
const User = db.user;
const Thresold = db.thresold;
const Request = db.request;

const sequelize = db.sequelize;

class UserController {

    getAllData = async (id) => {
        return await Request.findOne(
            {
                where: { userId: id },
                attributes: [
                    [sequelize.fn('sum', sequelize.col('quantityData')), 'totalQuantityData'],
                    [sequelize.fn('sum', sequelize.col('quantityCO2')), 'totalQuantityCO2'],
                ],
                group: ['userId'],
                include: User
            },
        );
    }

    getCurrentData = async (id) => {

        const currentMonth = new Date().getMonth() + 1;

        const res = await Request.findAll(
            {
                where: { userId: id },
                attributes: [
                    [sequelize.fn('sum', sequelize.col('quantityData')), 'totalQuantityData'],
                    [sequelize.fn('sum', sequelize.col('quantityCO2')), 'totalQuantityCO2'],
                    [sequelize.fn('MONTH', sequelize.col('date')), 'month'],
                ],
                group: ['userId', 'month'],
                include: {
                    model: User,
                    include: Thresold
                },
                order: [['month', 'DESC']],
            },
        );

        return res.find((data) => data.dataValues.month == currentMonth);
    }

    getDataByCategory = async (id) => {
        const currentMonth = new Date().getMonth() + 1;

        const res = await Request.findAll(
            {
                where: { userId: id },
                attributes: [
                    [sequelize.fn('sum', sequelize.col('quantityData')), 'totalQuantityData'],
                    [sequelize.fn('sum', sequelize.col('quantityCO2')), 'totalQuantityCO2'],
                    [sequelize.fn('MONTH', sequelize.col('date')), 'month'],
                ],
                group: ['userId', 'month', 'categoryId'],
                include: [User, Category],
            },
        );

        return res.filter((data) => data.dataValues.month == currentMonth);

    }

    getDataByHistorical = async (id) => {
        const res = await Request.findAll(
            {
                where: { userId: id },
                attributes: [
                    [sequelize.fn('sum', sequelize.col('quantityData')), 'totalQuantityData'],
                    [sequelize.fn('sum', sequelize.col('quantityCO2')), 'totalQuantityCO2'],
                    [sequelize.fn('MONTH', sequelize.col('date')), 'month'],
                    [sequelize.fn('YEAR', sequelize.col('date')), 'year']
                ],
                group: ['userId', 'month', 'year'],
                include: {
                    model: User,
                    include: Thresold
                },
                limit: 5,
                order: [['year', 'DESC'], ['month', 'DESC']],
            },
        );

        return res.reverse();
    }
}

module.exports = new UserController();