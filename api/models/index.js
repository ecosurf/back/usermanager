const { Sequelize } = require('sequelize');
const config = require('../../common/config');

const sequelize = new Sequelize(
    config.MYSQL_DB,
    config.MYSQL_USER,
    config.MYSQL_PWD,
    {
        host: config.MYSQL_HOST,
        dialect: 'mysql'
    }
)


try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./User.js")(sequelize);
db.category = require("./Category.js")(sequelize);
db.request = require("./Request.js")(sequelize);
db.thresold = require("./Thresold.js")(sequelize);

db.user.hasMany(db.request, { as: "requests" });
db.request.belongsTo(db.user);

db.category.hasMany(db.request, { as: "requests" });
db.request.belongsTo(db.category);

db.thresold.hasMany(db.user, { as: "users" });
db.user.belongsTo(db.thresold);

module.exports = db;