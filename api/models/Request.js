const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {

    const Request = sequelize.define('Request', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        quantityData: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        quantityCO2: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        codeCountry: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        url: {
            type: DataTypes.STRING,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false
        }
    });

    return Request;
}
