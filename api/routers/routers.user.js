const express = require('express');
const UserController = require('../controllers/controllers.user');

var router = express.Router();


module.exports = (app) => {

    /**
     * Retourne la quantité total de données utilisées pour un utilisateur, sans limite de temps
     * @route GET /user/total
     * @group user
     * @param {string} userId.query.required
     * @returns {object} 200
     * @returns {Error} 400
     */
    router.get('/total', async (req, res) => {
        const { userId } = req.query;

        try {
            const data = await UserController.getAllData(userId);
            res.status(200).json(data)
        } catch (err) {
            console.error("/user/total", err.message);
            res.status(400).json({ message: err.message });
        }
    });

    /**
     * Retourne la quantité total de données utilisées pour un utilisateur durant le mois courant
     * @route GET /user/current
     * @group user
     * @param {string} userId.query.required
     * @returns {object} 200
     * @returns {Error} 400
     */
    router.get('/current', async (req, res) => {
        const { userId } = req.query;

        try {
            const data = await UserController.getCurrentData(userId);
            res.status(200).json(data)
        } catch (err) {
            console.error("/user/total", err.message);
            res.status(400).json({ message: err.message });
        }
    });

    /**
     * Retourne la quantité total de données utilisées pour un utilisateur durant le mois courant par catégorie
     * @route GET /user/current/categories
     * @group user
     * @param {string} userId.query.required
     * @returns {object} 200
     * @returns {Error} 400
     */
    router.get('/current/categories', async (req, res) => {

        const { userId } = req.query;

        try {
            const data = await UserController.getDataByCategory(userId);
            res.status(200).json(data)
        } catch (err) {
            console.error("/user/categories", err.message);
            res.status(400).json({ message: err.message });
        }
    });

    /**
     * Retourne la quantité total de données utilisées pour un utilisateur par mois
     * @route GET /user/historical
     * @group user
     * @param {string} userId.query.required
     * @returns {object} 200
     * @returns {Error} 400
     */
    router.get('/historical', async (req, res) => {
        const { userId } = req.query;

        try {
            const data = await UserController.getDataByHistorical(userId);
            res.status(200).json(data)
        } catch (err) {
            console.error("/user/categories", err.message);
            res.status(400).json({ message: err.message });
        }
    })

    app.use('/user', router);
}