FROM node:18.10.0-slim

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

ENV TZ=Europe/Paris

COPY . .
RUN npm install
RUN npm ci --omit=dev

EXPOSE 8000
CMD [ "npm", "run", "start" ]
