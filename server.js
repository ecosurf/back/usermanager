require('dotenv').config()

const express = require('express');
const glob = require('glob');
const cors = require('cors');
const morgan = require('morgan');

const swaggerDoc = require('./common/swagger')
const config = require('./common/config');

const app = express();
const expressSwagger = require('express-swagger-generator')(app);

app.use(cors());
expressSwagger(swaggerDoc);
app.use(morgan('dev'))

const db = require("./api/models/index.js");
db.sequelize.sync();

// Définition des routers
files = glob.sync('api/routers/routers.*.js');
if (!files)
    console.error(`Error glob : ${files}`)
else {
    for (const file of files)
        require(`./${file}`)(app);
}

app.listen(config.PORT, '0.0.0.0', (err) => {
    if (err)
        console.err(`Error launch : ${err}`);
    else {
        console.log(`Serveur launch on : ${config.PORT}`);
        console.log(`Swagger on localhost:${config.PORT}/api-docs`)
    }
});
